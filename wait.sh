#!/bin/sh
rounds=18;
while [ $rounds -gt 0 ]; do
  set +e;
  curl -fsSL http://localhost | grep html && break;
  set -e;
  rounds=$(($rounds - 1));
  sleep 10;
done;
