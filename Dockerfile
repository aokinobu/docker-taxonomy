FROM registry.gitlab.com/aokinobu/docker-virtuoso:v7.2.4.2
MAINTAINER Nobuyuki Paul Aoki <aokinobu@gmail.com>

RUN apt-get update && \
  apt-get -y install curl && \
  apt-get clean -qq -y && \
  apt-get autoclean -qq -y && \
  apt-get autoremove -qq -y &&  \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /tmp/*
  
ADD ./taxonomy.ttl.gz /ttl/taxonomy.ttl.gz
ADD ./isqlload.sh /isqlload.sh
ADD ./load.sql /load.sql
ADD ./virtuoso.ini /virtuoso/db/virtuoso.ini
ADD ./wait.sh /wait.sh
ENV PATH="/usr/local/virtuoso-opensource/bin/:${PATH}"
#RUN ./isqlload.sh taxonomy.ttl.gz taxonomy.sparqlite.com

EXPOSE 8890 80
CMD ["/run.sh"]
